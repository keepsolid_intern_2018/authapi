<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;
use App\Models\User;
use Validator;
use Illuminate\Contracts\Encryption\DecryptException;

class SignInController extends Controller
{
    //
    public function auth(Request $request)
    {
        $requestParams = $request->only(
            'login',
            'password'
        );

        $validationRules = [
            'login' => 'required|email',
            'password' => 'required|min:8'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $hasToken = json_decode(Redis::get($requestParams['login']));
        if ($hasToken) {
            return (new Response([
                'code' => HTTP_FOUND,
                'message' => 'User are logged'
            ], HTTP_FOUND, ['description' => 'Access Forbidden']));
        }

        $user = User::select('users.*')
            ->where('users.email', '=', $requestParams['login'])
            ->where('users.password', '=', md5($requestParams['password']))
            ->first();

        if ($user) {
            $token = env('SECRET_KEY', 'bvh3f6yur8gh98fo') . $user['email'];
            $token = \Illuminate\Support\Facades\Crypt::encrypt($token);

            $redisValue = array($user['email'], $token, time() + env('SESSION_LIFETIME', '120')  * 60);

            Redis::set($user['email'], json_encode($redisValue));
            Redis::expireat($user['email'], time() + env('SESSION_LIFETIME', '120')  * 60);

            $redisValue= json_decode(Redis::get($user['email']));

            return (new Response([
                'id' => $user['id'],
                'email' => $user['email'],
                'first_name' => $user['first_name'],
                'last_name' => $user['last_name'],
                'city' => $user['city'],
                'session' => [
                    'id' => $redisValue[0],
                    'session' => $redisValue[1],
                    'expire_at' => $redisValue[2]
                ]
            ], HTTP_OK, ['description' => 'Authorization OK']));
        }
        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'Bad email or password'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
    }

    public function setPassword(Request $request)
    {
        $session = $request->header('SESSION-ID');
        if (!$session) {
            return (new Response([
                'code' => HTTP_BAD_REQUEST,
                'message' => 'Token Required'
            ], HTTP_BAD_REQUEST, ['description' => 'Token Required']));
        }

        try {
            $token = \Illuminate\Support\Facades\Crypt::decrypt($session);
            $token = substr($token, strlen(env('SECRET_KEY', 'bvh3f6yur8gh98fo')));

            $sessionInfo = json_decode(Redis::get($token));
            if (!$sessionInfo) {
                return (new Response([
                    'code' => HTTP_UNAUTHORIZED,
                    'message' => 'Session expired'
                ], HTTP_UNAUTHORIZED, ['description' => 'Authorization Required']));
            }
        }
        catch(DecryptException $e){
            return (new Response([
                'code' => HTTP_FORBIDDEN,
                'message' => 'Invalid Token were passed'
            ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
        }

        if ($sessionInfo[1] !== $session) {
            return (new Response([
                'code' => HTTP_UNAUTHORIZED,
                'message' => 'Session expired'
            ], HTTP_UNAUTHORIZED, ['description' => 'Authorization Required']));
        }

        $requestParams = $request->only(
            'password'
        );

        $validationRules = [
            'password' => 'required|min:8'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $user = User::where('users.email', '=', $sessionInfo[0])
            ->update(['password' => md5($requestParams['password'])]);

        if ($user) {
            return (new Response([
                'code' => HTTP_OK,
                'message' => 'New password was set'
            ], HTTP_OK, ['description' => 'Set password OK']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'New password was not set'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));

    }

    public function requestPasswordReset(Request $request)
    {
        $requestParams = $request->only(
            'email'
        );

        $validationRules = [
            'email' => 'required|email'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $user = User::where('users.email', '=', $requestParams['email'])
            ->select('email')
            ->first();

        if ($user) {
            //need send email with token
            //if registered service MailService
            //app('MailService)->sendResetPasswordMail($email);
            return (new Response([
                'code' => HTTP_OK,
                'message' => 'Reset message was requested'
            ], HTTP_OK, ['description' => 'Reset password OK']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'Email is not exist in DB'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
    }

    public function setPasswordByToken(Request $request, $by_token)
    {
       //return  var_dump($request->);
        $requestParams = $request->only(
            'password'
        );

        $validationRules = [
            'password' => 'required|min:8'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        if (!$by_token) {
            return (new Response([
                'code' => HTTP_BAD_REQUEST,
                'message' => 'Token Required'
            ], HTTP_BAD_REQUEST, ['description' => 'Token Required']));
        }

        try {
            $token = \Illuminate\Support\Facades\Crypt::decrypt($by_token);
            $token = substr($token, strlen(env('SECRET_KEY', 'bvh3f6yur8gh98fo')));

            $sessionInfo = json_decode(Redis::get($token));
            if (!$sessionInfo) {
                return (new Response([
                    'code' => HTTP_UNAUTHORIZED,
                    'message' => 'Session expired'
                ], HTTP_UNAUTHORIZED, ['description' => 'Authorization Required']));
            }
        }
        catch(DecryptException $e){
            return (new Response([
                'code' => HTTP_FORBIDDEN,
                'message' => 'Invalid Token were passed'
            ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
        }

        if ($sessionInfo[1] !==  $by_token) {
            return (new Response([
                'code' => HTTP_UNAUTHORIZED,
                'message' => 'Session expired'
            ], HTTP_UNAUTHORIZED, ['description' => 'Authorization Required']));
        }

        $user = User::where('users.email', '=', $sessionInfo[0])
            ->update(['password' => md5($requestParams['password'])]);

        if ($user) {
            return (new Response([
                'code' => HTTP_OK,
                'message' => 'New password was set'
            ], HTTP_OK, ['description' => 'Set password OK']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'New password was not set. Invalid Token were passed'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
    }

    public function getUserProfile(Request $request, $id){

        $session = $request->header('SESSION-ID');
        if (!$session) {
            return (new Response([
                'code' => HTTP_BAD_REQUEST,
                'message' => 'Token Required'
            ], HTTP_BAD_REQUEST, ['description' => 'Token Required']));
        }

        try {
            $token = \Illuminate\Support\Facades\Crypt::decrypt($session);
            $token = substr($token, strlen(env('SECRET_KEY', 'bvh3f6yur8gh98fo')));

            $sessionInfo = json_decode(Redis::get($token));
            if (!$sessionInfo) {
                return (new Response([
                    'code' => HTTP_UNAUTHORIZED,
                    'message' => 'Session expired'
                ], HTTP_UNAUTHORIZED, ['description' => 'Authorization Required']));
            }
        }
        catch(DecryptException $e){
            return (new Response([
                'code' => HTTP_FORBIDDEN,
                'message' => 'Invalid Token were passed'
            ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
        }

        if ($sessionInfo[1] !== $session) {
            return (new Response([
                'code' => HTTP_UNAUTHORIZED,
                'message' => 'Session expired'
            ], HTTP_UNAUTHORIZED, ['description' => 'Authorization Required']));
        }

        $requestParams = ['id' => $id];

        $validationRules = [
            'id' => 'required|integer|exists:users,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $user = User::where('users.id', '=', $id)->first();
        //return var_dump($user);

        if ($user->email === $sessionInfo[0]) {
            return (new Response([
                'id' => $user->id,
                'email' => $user->email,
                'name_first' => $user->first_name,
                'name_last' => $user->last_name,
                'city' => $user->city
            ], HTTP_OK, ['description' => 'Get profile OK']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'Get profile was not send'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));

    }

    public function setUserProfile(Request $request, $id){

        $session = $request->header('SESSION-ID');
        if (!$session) {
            return (new Response([
                'code' => HTTP_BAD_REQUEST,
                'message' => 'Token Required'
            ], HTTP_BAD_REQUEST, ['description' => 'Token Required']));
        }

        try {
            $token = \Illuminate\Support\Facades\Crypt::decrypt($session);
            $token = substr($token, strlen(env('SECRET_KEY', 'bvh3f6yur8gh98fo')));

            $sessionInfo = json_decode(Redis::get($token));
            if (!$sessionInfo) {
                return (new Response([
                    'code' => HTTP_UNAUTHORIZED,
                    'message' => 'Session expired'
                ], HTTP_UNAUTHORIZED, ['description' => 'Authorization Required']));
            }
        }
        catch(DecryptException $e){
            return (new Response([
                'code' => HTTP_FORBIDDEN,
                'message' => 'Invalid Token were passed'
            ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
        }

        if ($sessionInfo[1] !== $session) {
            return (new Response([
                'code' => HTTP_UNAUTHORIZED,
                'message' => 'Session expired'
            ], HTTP_UNAUTHORIZED, ['description' => 'Authorization Required']));
        }

        $requestParams = $request->only(
            'first_name',
            'last_name',
            'city'
        );
        $requestParams ['id'] = $id;

        $validationParams = [
            'id' => 'required|integer|exists:users,id',
            'first_name' => 'max:50|min:3',
            'last_name' => 'max:50|min:3',
            'city' => 'min:3'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationParams
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error)
                , HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $user = User::where('users.id', '=', $id)->first();

        if ($user->email === $sessionInfo[0]) {
        $res = User::where('users.id', '=', $id)
            ->update([
                'first_name' => $requestParams['first_name'],
                'last_name' => $requestParams['last_name'],
                'city' => $requestParams['city']
            ]);
        //return var_dump($user);

            if ($res) {
            return (new Response([
                'id' => $user->id,
                'email' => $user->email,
                'first_name' => $requestParams['first_name'],
                'last_name' => $requestParams['last_name'],
                'city' => $requestParams['city']
            ], HTTP_OK, ['description' => 'Set profile OK']));
        }}

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'Set profile was not set'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
    }
}
