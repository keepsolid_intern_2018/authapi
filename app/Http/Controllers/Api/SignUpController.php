<?php

namespace App\Http\Controllers\Api;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\User;
//use App\Services\SendMailService;

class SignUpController extends Controller
{
    //Register user
    public function signUpUser(Request $request)
    {
        $requestParams = $request->only(
            'email',
            'first_name',
            'last_name',
            'city',
            'password'
        );

        $validationParams = [
            'email' => 'required|email|unique:users,email',
            'first_name' => 'max:50|min:3',
            'last_name' => 'max:50|min:3',
            'city' => 'min:3',
            'password' => 'required|min:8'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationParams
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error)
                , HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }
        $userParams = [
            'first_name' => $requestParams['first_name'],
            'last_name' => $requestParams['last_name'],
            'city' => $requestParams['city'],
            'password' => md5($requestParams['password']),
            'email' => $requestParams['email']
        ];

        $existUser = DB::table('users')
            ->where('first_name','=',$requestParams['first_name'])
            ->where('last_name','=',$requestParams['last_name'])
            ->get();
        if (count($existUser) > 0) {
            return (new Response([
                'code' => HTTP_FOUND,
                'message' => 'User with these first_name and last_name already exists'
            ], HTTP_FOUND, ['description' => 'User exists']));
        }

        $user = new User($userParams);
        $user->save();

        //app('MailService')->sendRegistrationMail($email);

        return response(new Response([
            'id' => $user->id,
            'email' => $requestParams['email'],
            'name_first' => $requestParams['first_name'],
            'name_last' => $requestParams['last_name'],
            'city' => $requestParams['city']
        ], HTTP_OK, ['description' => 'Sign-Up OK']));
    }
}
