<?php

namespace App\Http\Controllers\Api;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class SessionController extends Controller
{
    //
    public function checkSession(Request $request)
    {
        $session = $request->header('SESSION-ID');
        if (!$session) {
            return (new Response([
                'code' => HTTP_BAD_REQUEST,
                'message' => 'Token Required'
            ], HTTP_BAD_REQUEST, ['description' => 'Token Required']));
        }

        try {
            $token = \Illuminate\Support\Facades\Crypt::decrypt($session);
            $token = substr($token, strlen(env('SECRET_KEY', 'bvh3f6yur8gh98fo')));

            $sessionInfo = json_decode(Redis::get($token));
            if (!$sessionInfo) {
                return (new Response([
                    'code' => HTTP_UNAUTHORIZED,
                    'message' => 'Session expired'
                ], HTTP_UNAUTHORIZED, ['description' => 'Authorization Required']));
            }
        }
        catch(DecryptException $e){
            return (new Response([
                'code' => HTTP_FORBIDDEN,
                'message' => 'Invalid Token were passed'
            ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
        }

        if ($sessionInfo[1] !== $session) {
            return (new Response([
                'code' => HTTP_UNAUTHORIZED,
                'message' => 'Session expired'
            ], HTTP_UNAUTHORIZED, ['description' => 'Authorization Required']));
        }

        if ($sessionInfo[2] < time() + env('SESSION_LIFETIME', '120')  * 30) {
            $expire = env('SESSION_LIFETIME', '120')  * 60;
            $sessionInfo[2] = time() + $expire;

            if (Redis::set($sessionInfo[0], json_encode($sessionInfo))) {
                Redis::expireat($sessionInfo[0], time() + env('SESSION_LIFETIME', '120')  * 60);
                return (new Response([
                    'id' => $sessionInfo[0],
                    'session' => $sessionInfo[1],
                    'expires_at' => $sessionInfo[2]
                ], HTTP_OK, ['description' => 'Session checked and prolonged']));
            }
            return (new Response([
                'code' => HTTP_FORBIDDEN,
                'message' => 'Session checked and not prolonged'
            ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
        }

        return (new Response([
            'id' => $sessionInfo[0],
            'session' => $sessionInfo[1],
            'expires_at' => $sessionInfo[2]
        ], HTTP_OK, ['description' => 'Session checked']));
    }

    public function expire(Request $request)
    {
        $session = $request->header('SESSION-ID');
        if (!$session) {
            return (new Response([
                'code' => HTTP_BAD_REQUEST,
                'message' => 'Token Required'
            ], HTTP_BAD_REQUEST, ['description' => 'Token Required']));
        }

        try {
            $token = \Illuminate\Support\Facades\Crypt::decrypt($session);
            $token = substr($token, strlen(env('SECRET_KEY', 'bvh3f6yur8gh98fo')));

            $sessionInfo = json_decode(Redis::get($token));

            if (!$sessionInfo) {
                return (new Response([
                    'code' => HTTP_UNAUTHORIZED,
                    'message' => 'Session expired'
                ], HTTP_UNAUTHORIZED, ['description' => 'Authorization Required']));
            }
        }
        catch(DecryptException $e){
            return (new Response([
                'code' => HTTP_FORBIDDEN,
                'message' => 'Invalid Token were passed'
            ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
        }

        if ($sessionInfo[1] !== $session) {
            return (new Response([
                'code' => HTTP_UNAUTHORIZED,
                'message' => 'Session expired'
            ], HTTP_UNAUTHORIZED, ['description' => 'Authorization Required']));
        }

        if (Redis::del($token)) {
            return (new Response([
                'code' => HTTP_OK,
                'message' => 'Session were expired'
            ], HTTP_OK, ['description' => 'Session were expired']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'Session not expired'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
    }
}
