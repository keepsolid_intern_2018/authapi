<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

$router->group(['prefix' => '/v1/user'], function ($router) {
    // Регистрация пользователей...
    $router->post('/signup', 'Api\SignUpController@signUpUser');
    // Авторизация пользователей
    $router->post('/auth', 'Api\SignInController@auth');
    // Работа с паролями
    $router->post('/set-password', 'Api\SignInController@setPassword');
    $router->post('/request-password-reset', 'Api\SignInController@requestPasswordReset');
    $router->post('/set-password/{by_token}', 'Api\SignInController@setPasswordByToken');
    // Работа с профилем
    $router->get('/profile/{id}', 'Api\SignInController@getUserProfile');
    $router->post('/profile/{id}', 'Api\SignInController@setUserProfile');

});

$router->group(['prefix' => '/v1/session'], function ($router) {
    // Работа с сессиями ...
    $router->post('/expire', 'Api\SessionController@expire');
    $router->post('/check', 'Api\SessionController@checkSession');
});

